
# Lab-8: Shell

### Version en francés

Créez Lab-8 avec 3 machines virtuelles en utilisant Vagrant et configurez-les selon vos spécifications :

1. **Créez un dossier lab-8** :
    ```
    mkdir lab-8
    cd lab-8
    ```
2. **Ajoutez les boîtes Vagrant nécessaires** : Utilisez la commande `vagrant box add` pour ajouter les boîtes Vagrant à votre environnement local. Assurez-vous d'ajouter la boîte "ubuntu/xenial64" ou toute autre boîte que vous avez choisie.

   ```
   vagrant box add ubuntu/xenial64
   ```

3. **Créez un Vagrantfile** :

    Créez un fichier nommé `Vagrantfile` dans le répertoire `lab-8` avec le contenu suivant :

    ```ruby
    Vagrant.configure("2") do |config|

      config.vm.define "lb" do |lb|
        lb.vm.box = "ubuntu/xenial64"
        lb.vm.hostname = "lb"
        lb.vm.network "private_network", ip: "10.0.0.10"
        lb.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        lb.vm.provision "shell", path: "lb_script.sh"
      end

      config.vm.define "web1" do |web1|
        web1.vm.box = "ubuntu/xenial64"
        web1.vm.hostname = "web1"
        web1.vm.network "private_network", ip: "10.0.0.11"
        web1.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        web1.vm.provision "shell", path: "web1_script.sh"
      end

      config.vm.define "web2" do |web2|
        web2.vm.box = "ubuntu/xenial64"
        web2.vm.hostname = "web2"
        web2.vm.network "private_network", ip: "10.0.0.12"
        web2.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        web2.vm.provision "shell", path: "web2_script.sh"
      end

    end
    ```

4. **Créez les scripts de provisionnement** :
   
   Vous devez créer trois scripts : `lb_script.sh`, `web1_script.sh`, et `web2_script.sh` avec le contenu que vous souhaitez exécuter sur chaque machine. Vous pouvez y mettre vos commandes de configuration ou d'installation.

5. **Accédez à l'interface web de chaque serveur** :

   Une fois que les machines virtuelles sont lancées, vous pouvez accéder à l'interface web de chaque serveur en utilisant leurs adresses IP respectives dans un navigateur web.

6. **Arrêtez les machines virtuelles et supprimez-les** :

   Une fois vos tests terminés, vous pouvez arrêter et supprimer les machines virtuelles en utilisant les commandes suivantes :

   ```bash
   vagrant halt
   vagrant destroy
   ```

   Ces commandes arrêteront et supprimeront toutes les machines virtuelles créées par Vagrant.

Assurez-vous d'avoir Vagrant et VirtualBox installés sur votre système avant de procéder à ces étapes. De plus, personnalisez les scripts de provisionnement selon vos besoins et assurez-vous qu'ils sont placés dans le même répertoire que le `Vagrantfile`.

### English version


Create Lab-8 with 3 VMs using Vagrant and configure them according to your specifications:

1. **Create a lab-8 folder**:
    ```
    mkdir lab-8
    cd lab-8
    ```
2. **Add necessary Vagrant boxes**: Use the `vagrant box add` command to add Vagrant boxes to your local environment. Make sure to add the "ubuntu/xenial64" box or any other box you have chosen.

   ```
   vagrant box add ubuntu/xenial64


3. **Create a Vagrantfile**:

    Create a file named `Vagrantfile` in the `lab-8` directory with the following content:

    ```ruby
    Vagrant.configure("2") do |config|

      config.vm.define "lb" do |lb|
        lb.vm.box = "ubuntu/xenial64"
        lb.vm.hostname = "lb"
        lb.vm.network "private_network", ip: "10.0.0.10"
        lb.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        lb.vm.provision "shell", path: "lb_script.sh"
      end

      config.vm.define "web1" do |web1|
        web1.vm.box = "ubuntu/xenial64"
        web1.vm.hostname = "web1"
        web1.vm.network "private_network", ip: "10.0.0.11"
        web1.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        web1.vm.provision "shell", path: "web1_script.sh"
      end

      config.vm.define "web2" do |web2|
        web2.vm.box = "ubuntu/xenial64"
        web2.vm.hostname = "web2"
        web2.vm.network "private_network", ip: "10.0.0.12"
        web2.vm.provider "virtualbox" do |vb|
          vb.memory = "1024"
          vb.cpus = 1
        end
        web2.vm.provision "shell", path: "web2_script.sh"
      end

    end
    ```

4. **Create the provision scripts**:
   
   You need to create three scripts: `lb_script.sh`, `web1_script.sh`, and `web2_script.sh` with the content you want to execute on each machine. You can put your desired setup or configuration commands in these files.

5. **Access each server's web interface**:

   Once the VMs are up and running, you can access each server's web interface using their respective IP addresses in a web browser.

6. **Stop the VMs and delete them**:

   Once you are done with your testing, you can stop and destroy the VMs using the following commands:

   ```bash
   vagrant halt
   vagrant destroy
   ```

   These commands will stop and remove all the VMs created by Vagrant.

Ensure you have Vagrant and VirtualBox installed on your system before you proceed with these steps. Also, customize the provision scripts according to your requirements and ensure they are placed in the same directory as the `Vagrantfile`.