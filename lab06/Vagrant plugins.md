### Version française : :

Lab-6 : Vagrant plugins

Pour compléter le Laboratoire-6 : Vagrant plugins, suivez ces étapes :

1. **Créez le dossier lab6** : Ouvrez votre terminal et exécutez la commande suivante pour créer un répertoire appelé lab6 où nous stockerons nos fichiers de configuration :
   ```
   mkdir lab6
   ```

2. **Accédez au répertoire lab5** : Naviguez dans le répertoire nouvellement créé en utilisant la commande suivante :
   ```
   cd lab6
   ```

3. **Ajoutez les boîtes Vagrant nécessaires** : Utilisez la commande `vagrant box add` pour ajouter les boîtes Vagrant à votre environnement local. Assurez-vous d'ajouter la boîte "ubuntu/xenial64" ou toute autre boîte que vous avez choisie.
   ```
   vagrant box add ubuntu/xenial64
   ```

4. **Créez un fichier Vagrantfile et ouvrez-le avec un éditeur de texte** : Utilisez la commande suivante pour créer un fichier Vagrantfile dans le répertoire lab6 et ouvrez-le avec votre éditeur de texte préféré.
   ```
   vagrant init --minimal
   ```
   ```
   code Vagrantfile
   ```

5. **Ajoutez le contenu au fichier Vagrantfile** : Copiez et collez le contenu suivant dans le fichier Vagrantfile que vous venez d'ouvrir avec votre éditeur de texte :

```ruby
Vagrant.configure("2") do |config|

  # Configuration of the lb machine (Load Balancer)
  config.vm.define "lb" do |lb|
    lb.vm.box = "ubuntu/xenial64"
    lb.vm.hostname = "lb"
    lb.vm.network "private_network", ip: "10.0.0.10"
    lb.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end
  end

  # Configuration of the web1 machine
  config.vm.define "web1" do |web1|
    web1.vm.box = "ubuntu/xenial64"
    web1.vm.hostname = "web1"
    web1.vm.network "private_network", ip: "10.0.0.11"
    web1.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web1.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web1</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

  # Configuration of the web2 machine
  config.vm.define "web2" do |web2|
    web2.vm.box = "ubuntu/xenial64"
    web2.vm.hostname = "web2"
    web2.vm.network "private_network", ip: "10.0.0.12"
    web2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web2.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web2</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

   # Installation and configuration of the vagrant-hostupdater plugin
   if Vagrant.has_plugin?("vagrant-hostsupdater")
    config.hostsupdater.remove_on_suspend = false
  else
    puts "The vagrant-hostupdater plugin is not installed. Please install it using 'vagrant plugin install vagrant-hostsupdater'."
  end

end
```

6. **Pour installer et utiliser le plugin vagrant-hostupdater** :

Ouvrez un terminal ou une ligne de commande.
Allez dans le répertoire lab-6 que vous avez créé.
Exécutez la commande suivante pour installer le plugin :
```
vagrant plugin install vagrant-hostsupdater
```
Cette commande installera le plugin vagrant-hostsupdater sur votre système.

Une fois que vous avez installé le plugin, en exécutant vagrant up, le plugin mettra automatiquement à jour le fichier hosts de votre système pour refléter les adresses IP et les noms d'hôtes des machines virtuelles que vous créez.

**Comment cela fonctionne-t-il ?**

Le plugin vagrant-hostsupdater automatise la gestion du fichier `/etc/hosts` du système hôte lorsqu'on travaille avec Vagrant. Voici en quoi il consiste brièvement :

- **Pendant les commandes Up, Resume et Reload** : Ajoute une entrée au fichier `/etc/hosts` si elle n'est pas déjà présente. Cette entrée contient des informations sur les machines virtuelles créées par Vagrant. Si l'entrée doit être ajoutée, le plugin demande le mot de passe de l'administrateur car il utilise `sudo` pour éditer le fichier.

- **Pendant les commandes Halt, Destroy et Suspend** : Les entrées ajoutées par le plugin sont supprimées du fichier `/etc/hosts`. Cependant, si vous définissez `config.hostsupdater.remove_on_suspend = false`, les entrées ne seront pas supprimées pendant les opérations de suspension ou d'arrêt.

En résumé, le plugin garantit que votre système hôte puisse communiquer facilement avec les machines virtuelles gérées par Vagrant en mettant automatiquement à jour le fichier `/etc/hosts` selon les besoins, et permet également de personnaliser son comportement via des options de configuration telles que `remove_on_suspend`.

7. **Connectez-vous aux machines virtuelles et activez la page web** :
   -Lancer machine virtuelle : 
   ```
   vagrant up
   ```
   
    Connectez-vous à chaque machine virtuelle via SSH à l'aide de la commande 
    ```
    vagrant ssh nom_de_la_machine  
    ```

   - Assurez-vous que les machines virtuelles web1 et web2 ont une page web active.

8. **Arrêtez et supprimez les machines virtuelles** :
   - Une fois que vous avez terminé avec vos machines virtuelles, utilisez la commande 
   ```
   vagrant halt
   ```
   
   ` pour les arrêter.
   - Ensuite, utilisez la commande 
   ```
   vagrant destroy  
   ```
    pour supprimer définitivement les machines virtuelles de votre environnement.


### English version:

Lab-6: Vagrant plugins

To complete Lab-6: Vagrant plugins, follow these steps:

1. **Create the lab6 folder**: Open your terminal and execute the following command to create a directory called lab6 where we will store our configuration files:
   ```
   mkdir lab6
   ```

2. **Navigate to the lab6 directory**: Navigate to the newly created directory using the following command:
   ```
   cd lab6
   ```

3. **Add necessary Vagrant boxes**: Use the `vagrant box add` command to add Vagrant boxes to your local environment. Make sure to add the "ubuntu/xenial64" box or any other box you have chosen.
   ```
   vagrant box add ubuntu/xenial64
   ```

4. **Create a Vagrantfile and open it with a text editor**: Use the following command to create a Vagrantfile in the lab6 directory and open it with your preferred text editor.
   ```
   vagrant init --minimal
   ```
   ```
   notepad Vagrantfile
   ```

5. **Add content to the Vagrantfile**: Copy and paste the following content into the Vagrantfile that you just opened with your text editor:

```Ruby
# Lab-6: Vagrant plugins

Vagrant.configure("2") do |config|
  
Vagrant.configure("2") do |config|

  # Configuration of the lb machine (Load Balancer)
  config.vm.define "lb" do |lb|
    lb.vm.box = "ubuntu/xenial64"
    lb.vm.hostname = "lb"
    lb.vm.network "private_network", ip: "10.0.0.10"
    lb.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end
  end

  # Configuration of the web1 machine
  config.vm.define "web1" do |web1|
    web1.vm.box = "ubuntu/xenial64"
    web1.vm.hostname = "web1"
    web1.vm.network "private_network", ip: "10.0.0.11"
    web1.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web1.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web1</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

  # Configuration of the web2 machine
  config.vm.define "web2" do |web2|
    web2.vm.box = "ubuntu/xenial64"
    web2.vm.hostname = "web2"
    web2.vm.network "private_network", ip: "10.0.0.12"
    web2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web2.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web2</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

   # Installation and configuration of the vagrant-hostupdater plugin
   if Vagrant.has_plugin?("vagrant-hostsupdater")
    config.hostsupdater.remove_on_suspend = false
  else
    puts "The vagrant-hostupdater plugin is not installed. Please install it using 'vagrant plugin install vagrant-hostsupdater'."
  end

end
```

6. **To install and use the vagrant-hostupdater plugin**:

Execute the following command to install the plugin:
```
vagrant plugin install vagrant-hostsupdater
```
This command will install the vagrant-hostsupdater plugin on your system.

Once you have installed the plugin, by running vagrant up, the plugin will automatically update your system's hosts file to reflect the IP addresses and host names of the virtual machines you create.

**How does it work?**

The vagrant-hostsupdater plugin automates the management of the host system's `/etc/hosts` file when working with Vagrant. Here's what it does briefly:

- **During Up, Resume, and Reload commands**: Adds an entry to the `/etc/hosts` file if it's not already present. This entry contains information about the virtual machines created by Vagrant. If the entry needs to be added, the plugin requests administrator password since it uses `sudo` to edit the file.

- **During Halt, Destroy, and Suspend commands**: The entries added by the plugin are removed from the `/etc/hosts` file. However, if you set `config.hostsupdater.remove_on_suspend = false`, entries won't be removed during suspend or halt operations.

In summary, the plugin ensures that your host system can easily communicate with the virtual machines managed by Vagrant by updating the `/etc/hosts` file accordingly, and it also allows for customization of its behavior through configuration options like `remove_on_suspend`.



7. **Connect to the virtual machines and activate the web page**:
   - Launch the virtual machine   

   ```
   vagrant up
  
   ```
   - Connect to each virtual machine via SSH using the command 

   ```
   vagrant ssh machine_name

   ```
   - Ensure that the web1 and web2 virtual machines have an active web page.

8. **Stop and remove the virtual machines**:
   - Once you are done with your virtual machines, use 
   
    ```
   vagrant halt
   ``` 
   command to stop them.
   - Then, use the command

    ```
    vagrant destroy
    ``` 
    To permanently remove the virtual machines from your environment.


