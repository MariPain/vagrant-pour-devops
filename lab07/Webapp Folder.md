### Version française : 

## Lab-7 : Webapp Folder

Pour compléter le Laboratoire-7 : Webapp Folder, suivez ces étapes :

1. **Créez le dossier lab7** : Ouvrez votre terminal et exécutez la commande suivante pour créer un répertoire appelé lab où nous stockerons nos fichiers de configuration :
   ```
   mkdir lab7
   ```

2. **Accédez au répertoire lab7** : Naviguez dans le répertoire nouvellement créé en utilisant la commande suivante :
   ```
   cd lab7
   ```

3. **Ajoutez les boîtes Vagrant nécessaires** : Utilisez la commande `vagrant box add` pour ajouter les boîtes Vagrant à votre environnement local. Assurez-vous d'ajouter la boîte "ubuntu/xenial64" ou toute autre boîte que vous avez choisie.
   ```
   vagrant box add mariaasdram/nginx --box-version 2.0
   ```

4. **Créez un fichier Vagrantfile et ouvrez-le avec un éditeur de texte** : Utilisez la commande suivante pour créer un fichier Vagrantfile dans le répertoire lab6 et ouvrez-le avec votre éditeur de texte préféré.
   ```
   vagrant init --minimal
   ```
   ```
   code Vagrantfile
   ```

5. **Ajoutez le contenu au fichier Vagrantfile** : Copiez et collez le contenu suivant dans le fichier Vagrantfile que vous venez d'ouvrir avec votre éditeur de texte :

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "mariaasdram/nginx"
  config.vm.box_version = "1"

  # Configuration of CPU and RAM quantity
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
    vb.cpus = 1
  end

  # Configuration of the network with a fixed private IP
  config.vm.network "private_network", ip: "10.0.0.10"
  config.vm.hostname = "webserver" 

  # SSH connection configuration
  config.ssh.insert_key = true

  # Folder sharing to mount application code
  config.vm.synced_folder "%USERPROFILE%/Documents/EJERCICIOS/DevOps/Vagrant-pour-Devops/lab7/site_static", "/var/www/html"

  # Provisioner to create /var/www/html directory if it doesn't exist
  config.vm.provision "shell", inline: "sudo mkdir -p /var/www/html"
end

```

6. **Connectez-vous aux machines virtuelles et activez la page web** :
   -Lancer machine virtuelle : 
   ```
   vagrant up
   ```
   
   Connectez-vous à chaque machine virtuelle via SSH à l'aide de la commande 
    ```
   vagrant ssh   
    ```

   - Assurez-vous que les machines virtuelles web1 et web2 ont une page web active.

7. **Arrêtez et supprimez les machines virtuelles** :
   - Une fois que vous avez terminé avec vos machines virtuelles, utilisez la commande 
   ```
   vagrant halt
   ```
   
   ` pour les arrêter.
   - Ensuite, utilisez la commande 
   ```
   vagrant destroy  
   ```
    pour supprimer définitivement les machines virtuelles de votre environnement.


### English version:

Lab-6: Webapp Folder

To complete Lab-7: Webapp Folder:

1. **Create the lab7 folder**: Open your terminal and execute the following command to create a directory called lab6 where we will store our configuration files:
   ```
   mkdir lab7
   ```

2. **Navigate to the lab6 directory**: Navigate to the newly created directory using the following command:

   ```
   cd lab7
   ```

3. **Add necessary Vagrant boxes**: Use the `vagrant box add` command to add Vagrant boxes to your local environment. Make sure to add the "ubuntu/xenial64" box or any other box you have chosen.

   ```
   vagrant box add mariaasdram/nginx  --box-version 1

   ```

4. **Create a Vagrantfile and open it with a text editor**: Use the following command to create a Vagrantfile in the lab7 directory and open it with your preferred text editor.

   ```
   vagrant init --minimal
   ```
   ```
   code Vagrantfile
   ```

5. **Add content to the Vagrantfile**: Copy and paste the following content into the Vagrantfile that you just opened with your text editor:

```Ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "mariaasdram/nginx"
  config.vm.box_version = "1"

  # Configuration of CPU and RAM quantity
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
    vb.cpus = 1
  end

  # Configuration of the network with a fixed private IP
  config.vm.network "private_network", ip: "10.0.0.10"
  config.vm.hostname = "webserver" 

  # SSH connection configuration
  config.ssh.insert_key = true

  # Folder sharing to mount application code
  config.vm.synced_folder "%USERPROFILE%/Documents/EJERCICIOS/DevOps/Vagrant-pour-Devops/lab7/site_static"
, "/var/www/html"

  # Provisioner to create /var/www/html directory if it doesn't exist
  config.vm.provision "shell", inline: "sudo mkdir -p /var/www/html"
end


```


6. **Connect to the virtual machines and activate the web page**:
   - Launch the virtual machine   

   ```
   vagrant up
  
   ```
   - Connect to each virtual machine via SSH using the command 

   ```
   vagrant ssh

   ```
   - Ensure that the web1 and web2 virtual machines have an active web page.

7. **Stop and remove the virtual machines**:
   - Once you are done with your virtual machines, use 
   
    ```
   vagrant halt
   ``` 
   command to stop them.
   - Then, use the command

    ```
    vagrant destroy
    ``` 
    To permanently remove the virtual machines from your environment.


