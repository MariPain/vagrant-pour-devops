#!/bin/bash

# Script to automate Vagrant commands
# This script initializes Vagrant, validates the configuration, brings up the virtual machine,
# checks the status of the virtual machine, lists global status of all Vagrant environments,
# connects to the virtual machine via SSH, halts the virtual machine, and destroys the virtual machine.

# Step 1: Vagrant init
echo "Initializing Vagrant..."
vagrant init

# Step 2: Vagrant validate
echo "Validating Vagrant configuration..."
vagrant validate

# Step 3: Vagrant up
echo "Bringing up the virtual machine..."
vagrant up

# Step 4: Vagrant status
echo "Checking the status of the virtual machine..."
vagrant status

# Step 5: Vagrant global-status
echo "Listing global status of all Vagrant environments..."
vagrant global-status

# Step 6: Vagrant ssh
echo "Connecting to the virtual machine via SSH..."
vagrant ssh

# Step 7: Vagrant halt
echo "Halting the virtual machine..."
vagrant halt

# Step 8: Vagrant destroy
echo "Destroying the virtual machine..."
vagrant destroy

# Script execution completed
echo "Script execution completed."
