### Guide d'utilisation de Vagrant

#### Marche à suivre pour utiliser Vagrant

#### Vagrant init
- La commande `vagrant init` est utilisée pour initialiser un nouveau fichier Vagrantfile dans le répertoire courant.
- Le fichier Vagrantfile est un fichier de configuration utilisé par Vagrant pour décrire les caractéristiques de la machine virtuelle que vous souhaitez créer et gérer.

#### Vagrant validate
- La commande `vagrant validate` est utilisée pour valider la syntaxe et la configuration du fichier Vagrantfile.
- Cette commande vérifie si le fichier Vagrantfile est correctement écrit et ne contient pas d'erreurs de syntaxe.

#### Vagrant up
- La commande `vagrant up` est utilisée pour démarrer et provisionner la machine virtuelle en fonction des spécifications du fichier Vagrantfile.
- Cependant, il est possible de rencontrer une erreur lors de l'exécution de cette commande si Vagrant ne parvient pas à trouver le fichier de la box de base nécessaire à la création de la machine virtuelle.

**Pourquoi l'erreur se produit-elle ?**
- L'erreur se produit parce que Vagrant ne peut pas trouver le fichier de la box de base nécessaire à la création de la machine virtuelle. Par défaut, Vagrant tente de télécharger une box appelée "base" s'il ne trouve pas la box spécifiée dans le fichier Vagrantfile.

**Que devez-vous modifier dans le fichier Vagrantfile ?**
- Pour résoudre cette erreur, vous devez modifier le fichier Vagrantfile pour spécifier la box que vous souhaitez utiliser comme base pour votre machine virtuelle. Vous devez modifier la ligne `config.vm.box = "base"` pour spécifier le nom de la box que vous avez téléchargée ou que vous souhaitez utiliser.

#### Autres commandes Vagrant

##### Vagrant status
- `vagrant status` affiche l'état actuel de toutes les machines virtuelles gérées par Vagrant dans le répertoire courant.

##### Vagrant global-status
- `vagrant global-status` affiche l'état de toutes les machines virtuelles gérées par Vagrant, même si elles ne se trouvent pas dans le répertoire courant.

##### Vagrant ssh
- `vagrant ssh` se connecte à la machine virtuelle via SSH.

##### Vagrant halt
- `vagrant halt` arrête la machine virtuelle.

##### Vagrant destroy
- `vagrant destroy` arrête et supprime la machine virtuelle.

#### Téléchargement de nouvelles boxes et modification du fichier Vagrantfile

##### Vagrant box add ubuntu/trusty64
- `vagrant box add ubuntu/trusty64` télécharge et ajoute la box "ubuntu/trusty64" à votre système.

##### Vagrant init ubuntu/trusty64 -h
- `vagrant init ubuntu/trusty64 -h` initialise un nouveau fichier Vagrantfile en utilisant la box "ubuntu/trusty64" comme base.

**Comment télécharger ces boxes et modifier le fichier Vagrantfile ?**
- Pour télécharger une box, utilisez la commande `vagrant box add` suivie du nom de la box que vous souhaitez télécharger.
- Pour spécifier la box à utiliser dans le fichier Vagrantfile, modifiez la ligne `config.vm.box` pour faire référence au nom de la box que vous avez téléchargée.
- Assurez-vous également de spécifier le fournisseur de virtualisation que vous souhaitez utiliser, par exemple, VirtualBox.

##### Optionel

**Création de script d'automatisation pour l'initialisation de Vagrant**

```bash
#!/bin/bash

# Script to automate Vagrant commands
# This script initializes Vagrant, validates the configuration, brings up the virtual machine,
# checks the status of the virtual machine, lists global status of all Vagrant environments,
# connects to the virtual machine via SSH, halts the virtual machine, and destroys the virtual machine.

# Step 1: Vagrant init
echo "Initializing Vagrant..."
vagrant init

# Step 2: Vagrant validate
echo "Validating Vagrant configuration..."
vagrant validate

# Step 3: Vagrant up
echo "Bringing up the virtual machine..."
vagrant up

# Step 4: Vagrant status
echo "Checking the status of the virtual machine..."
vagrant status

# Step 5: Vagrant global-status
echo "Listing global status of all Vagrant environments..."
vagrant global-status

# Step 6: Vagrant ssh
echo "Connecting to the virtual machine via SSH..."
vagrant ssh

# Step 7: Vagrant halt
echo "Halting the virtual machine..."
vagrant halt

# Step 8: Vagrant destroy
echo "Destroying the virtual machine..."
vagrant destroy

# Script execution completed
echo "Script execution completed."
```

Enregistrez ce script dans un fichier, par exemple, `vagrant_script.sh`. Après l'enregistrement, vous devez le rendre exécutable en utilisant la commande suivante :

```bash
chmod +x vagrant_script.sh
```

Ensuite, vous pouvez exécuter le script en appelant `
