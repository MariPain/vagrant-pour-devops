### En français :

Étapes pour créer un Vagrantfile minimaliste à partir de la ligne de commande et ajouter des configurations avec un éditeur de texte :

1. Créez le dossier lab-3 : `mkdir lab-3`
2. Déplacez-vous dans ce dossier : `cd lab-3`
3. Utilisez la commande `vagrant init` pour créer un Vagrantfile minimaliste : `vagrant init --minimal`
4. Utilisez un éditeur de texte (comme Notepad) pour éditer le fichier Vagrantfile nouvellement créé.
5. Ajoutez les configurations suivantes dans le fichier Vagrantfile :

Vagrant.configure("2") do |config|
  config.vm.box = "geerlingguy/centos7"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end
  config.vm.provision "shell", inline: <<-SHELL
    sudo yum install -y epel-release
    sudo yum install -y nginx
    sudo systemctl enable nginx
    sudo systemctl start nginx
  SHELL
end

6. Enregistrez et fermez le fichier.
7. Vous pouvez maintenant lancer votre machine virtuelle en exécutant `vagrant up`.

8. Une fois que la machine virtuelle est en cours d'exécution, vous pouvez vous y connecter en utilisant `vagrant ssh`. Une fois connecté à la machine virtuelle via SSH, vous pouvez installer les logiciels nécessaires comme Nginx à l'intérieur de la machine virtuelle.
9. Après avoir terminé d'utiliser la machine virtuelle, quittez la session SSH en tapant `exit` dans le terminal.
10. Pour arrêter la machine virtuelle, exécutez la commande `vagrant halt` pour l'arrêter.
11. Enfin, pour supprimer complètement la machine virtuelle et toutes ses ressources associées, utilisez la commande `vagrant destroy`. Cela garantira la suppression complète de la machine virtuelle et de ses fichiers.

### In English:

Steps for creating a minimalist Vagrantfile from the command line and adding configurations with a text editor:

1. Create the lab-3 folder: `mkdir lab-3`.
2. Move to this folder: `cd lab-3`.
3. Use the `vagrant init` command to create a minimalist Vagrantfile: `vagrant init --minimal`.
4. Use a text editor (such as Notepad) to edit the newly created Vagrantfile.
5. Add the following configurations to the Vagrantfile:


Vagrant.configure("2") do |config|
  config.vm.box = "geerlingguy/centos7
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048
    vb.cpus = 2
  end
  config.vm.provision "shell", inline: <<-SHELL
    sudo yum install -y epel-release
    sudo yum install -y nginx
    sudo systemctl enable nginx
    sudo systemctl start nginx
  SHELL
end


6. Save and close the file.
7. You can now launch your virtual machine by running `vagrant up`.

8. Once the virtual machine is running, you can connect to it using `vagrant ssh`. Once connected to the virtual machine via SSH, you can install the necessary software such as Nginx inside the virtual machine.
9. When you've finished using the virtual machine, quit the SSH session by typing `exit` in the terminal.
10. To stop the virtual machine, execute the `vagrant halt` command.
11. Finally, to completely remove the virtual machine and all its associated resources, use the `vagrant destroy` command. This will ensure complete deletion of the virtual machine and its files.
