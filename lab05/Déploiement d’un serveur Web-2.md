### En français :

Lab-5: Déploiement d'un serveur Web

Pour compléter le Laboratoire-5 : Déploiement d'un serveur web, suivez ces étapes :

1. **Créez le dossier lab5** : Ouvrez votre terminal et exécutez la commande suivante pour créer un répertoire appelé lab5 où nous stockerons nos fichiers de configuration :
   ```
   mkdir lab5
   ```

2. **Accédez au répertoire lab5** : Naviguez dans le répertoire nouvellement créé en utilisant la commande suivante :
   ```
   cd lab5
   ```

3. **Ajoutez les boîtes Vagrant nécessaires** : Utilisez la commande `vagrant box add` pour ajouter les boîtes Vagrant à votre environnement local. Assurez-vous d'ajouter la boîte "ubuntu/xenial64" ou toute autre boîte que vous avez choisie.
   ```
   vagrant box add ubuntu/xenial64
   ```

4. **Créez un fichier Vagrantfile et ouvrez-le avec un éditeur de texte** : Utilisez la commande suivante pour créer un fichier Vagrantfile dans le répertoire lab5 et ouvrez-le avec votre éditeur de texte préféré.
   ```
   vagrant init --minimal
   ```
   ```
   notepad Vagrantfile
   ```

5. **Ajoutez le contenu au fichier Vagrantfile** : Copiez et collez le contenu suivant dans le fichier Vagrantfile que vous venez d'ouvrir avec votre éditeur de texte :

```ruby
Vagrant.configure("2") do |config|

  # Configuration of the lb machine (Load Balancer)
  config.vm.define "lb" do |lb|
    lb.vm.box = "ubuntu/xenial64"
    lb.vm.hostname = "lb"
    lb.vm.network "private_network", ip: "10.0.0.10"
    lb.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end
  end

  # Configuration of the web1 machine
  config.vm.define "web1" do |web1|
    web1.vm.box = "ubuntu/xenial64"
    web1.vm.hostname = "web1"
    web1.vm.network "private_network", ip: "10.0.0.11"
    web1.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web1.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web1</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

  # Configuration of the web2 machine
  config.vm.define "web2" do |web2|
    web2.vm.box = "ubuntu/xenial64"
    web2.vm.hostname = "web2"
    web2.vm.network "private_network", ip: "10.0.0.12"
    web2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web2.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web2</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

end

```


6. **Connectez-vous aux machines virtuelles et activez la page web** :
   -Lancer machine virtuelle `vagran up`
   - Connectez-vous à chaque machine virtuelle via SSH à l'aide de la commande `vagrant ssh nom_de_la_machine`.
   - Assurez-vous que les machines virtuelles web1 et web2 ont une page web active.

7. **Arrêtez et supprimez les machines virtuelles** :
   - Une fois que vous avez terminé avec vos machines virtuelles, utilisez la commande `vagrant halt` pour les arrêter.
   - Ensuite, utilisez la commande `vagrant destroy` pour supprimer définitivement les machines virtuelles de votre environnement.

### In English:

Lab-5: Web Server Deployment

To complete Lab-5: Web Server Deployment, follow these steps:

1. **Create the lab5 directory**: Open your terminal and execute the following command to create a directory named lab5 where we will store our configuration files:
   ```
   mkdir lab5
   ```

2. **Navigate to the lab5 directory**: Navigate into the newly created directory using the following command:
   ```
   cd lab5
   ```

3. **Add the necessary Vagrant boxes**: Use the `vagrant box add` command to add Vagrant boxes to your local environment. Make sure to add the "ubuntu/xenial64" box or any other box you have chosen.
   ```
   vagrant box add ubuntu/xenial64
   ```

4. **Create a Vagrantfile and open it with a text editor**: Use the following command to create a Vagrantfile in the lab5 directory and open it with your preferred text editor.
   ```
   vagrant init --minimal
   ```
   ```
   notepad Vagrantfile
   ```

5. **Add content to the Vagrantfile**: Copy and paste the following content into the Vagrantfile you just opened with your text editor:

```Vagrant.configure("2") do |config|

  # Configuration of the lb machine (Load Balancer)
  config.vm.define "lb" do |lb|
    lb.vm.box = "ubuntu/xenial64"
    lb.vm.hostname = "lb"
    lb.vm.network "private_network", ip: "10.0.0.10"
    lb.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end
  end

  # Configuration of the web1 machine
  config.vm.define "web1" do |web1|
    web1.vm.box = "ubuntu/xenial64"
    web1.vm.hostname = "web1"
    web1.vm.network "private_network", ip: "10.0.0.11"
    web1.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web1.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web1</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

  # Configuration of the web2 machine
  config.vm.define "web2" do |web2|
    web2.vm.box = "ubuntu/xenial64"
    web2.vm.hostname = "web2"
    web2.vm.network "private_network", ip: "10.0.0.12"
    web2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end

    web2.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y nginx
      echo "<h1>Welcome to Web2</h1>" | sudo tee /var/www/html/index.html
      sudo systemctl restart nginx
    SHELL
  end

end

```

6. **Connect to the virtual machines and enable the web page**:
   - Virtual machine up `vagrant up`.
   - Connect to each virtual machine via SSH using the `vagrant ssh` command + name of the machine `lb`, `web1` or `web2`.
   - Ensure that virtual machines web1 and web2 have an active web page.

7. **Stop and remove the virtual machines**:
   - Once you're done with your virtual machines, use the `vagrant halt` command to stop them.
   - Then, use the `vagrant destroy` command to permanently remove the virtual machines from your environment.