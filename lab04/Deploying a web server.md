### En français :

Pour compléter le Laboratoire-4 : Déploiement d'un serveur web, suivez ces étapes :

1. **Créez le dossier lab4** : Ouvrez votre terminal et exécutez la commande suivante pour créer un répertoire appelé lab4 où nous stockerons nos fichiers de configuration :
   ```
   mkdir lab4
   ```

2. **Accédez au répertoire lab4** : Naviguez dans le répertoire nouvellement créé en utilisant la commande suivante :
   ```
   cd lab4
   ```

3. **Créez un fichier Vagrantfile et ouvrez-le avec un éditeur de texte** : Utilisez la commande suivante pour créer un fichier Vagrantfile dans le répertoire lab4 et ouvrez-le avec votre éditeur de texte préféré.

   ```
   vagrant init --minimal
   ```
   ```
   notepad Vagrantfile
   ```

4. **Ajoutez le contenu au fichier Vagrantfile** : Copiez et collez le contenu suivant dans le fichier Vagrantfile que vous venez d'ouvrir avec votre éditeur de texte :

```ruby
Vagrant.configure("2") do |config|
  # Configuration de la box de base
  config.vm.box = "geerlingguy/centos7"

  # Configuration de la quantité de CPU et de RAM
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  # Configuration du réseau avec une IP privée fixe
  config.vm.network "private_network", ip: "10.0.0.10"

  # Configuration de la connexion SSH
  config.ssh.insert_key = false

  # Provisión pour installer Nginx
  config.vm.provision "shell", inline: <<-SHELL
    sudo yum install -y epel-release
    sudo yum install -y nginx
    sudo systemctl enable nginx
    sudo systemctl start nginx
  SHELL
end
```

5. **Enregistrez et fermez le fichier Vagrantfile** : Enregistrez les modifications apportées au fichier Vagrantfile et fermez-le.

6. **Démarrez la machine virtuelle avec Vagrant** : Exécutez la commande suivante pour démarrer la machine virtuelle avec Vagrant, qui configurera la machine selon les spécifications fournies dans le Vagrantfile :
   ```
   vagrant up
   ```

7. **Accédez à la machine virtuelle via SSH avec Vagrant** : Une fois la machine virtuelle en fonctionnement, vous pouvez vous y connecter via SSH en utilisant la commande suivante :
   ```
   vagrant ssh
   ```

8. **Accédez à l'application depuis votre PC local** : Vérifiez que vous pouvez accéder à l'application depuis votre PC local en saisissant http://10.0.0.10 dans un navigateur web.

9. **Arrêtez la machine virtuelle** : Lorsque vous avez fini de tester l'application, arrêtez la machine virtuelle avec la commande suivante :
   ```
   vagrant halt
   ```

10. **Supprimez la machine virtuelle et ses ressources associées** : Enfin, supprimez la machine virtuelle et toutes ses ressources associées avec la commande suivante :
    ```
    vagrant destroy
    ```

Ces étapes devraient vous guider à travers le Laboratoire-4 : Déploiement d'un serveur web en utilisant Vagrant. Si vous avez des questions ou avez besoin de plus d'aide, n'hésitez pas à demander !

### In English:

To complete Laboratory-4: Deployment of a web server, follow these steps:

1. **Create the lab4 folder**: Open your terminal and execute the following command to create a directory named lab4 where we will store our configuration files:
   ```
   mkdir lab4
   ```

2. **Enter the lab4 directory**: Navigate to the newly created directory using the following command:
   ```
   cd lab4
   ```

3. **Create a Vagrantfile and open it with a text editor**: Use the following command to create a Vagrantfile in the lab4 directory and open the file with your preferred text editor.
   ```
   notepad Vagrantfile
   ```

4. **Add content to the Vagrantfile**: Copy and paste the following content into the Vagrantfile that you just opened with your text editor:

```ruby
Vagrant.configure("2") do |config|
  # Configuration of the base box
  config.vm.box = "geerlingguy/centos7"

  # Configuration of CPU and RAM quantity
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  # Configuration of the network with a fixed private IP
  config.vm.network "private_network", ip: "10.0.0.10"

  # SSH connection configuration
  config.ssh.insert_key = false

  # Provisioning to install Nginx
  config.vm.provision "shell", inline: <<-SHELL
    sudo yum install -y epel-release
    sudo yum install -y nginx
    sudo systemctl enable nginx
    sudo systemctl start nginx
  SHELL
end
```

5. **Save and close the Vagrantfile**: Save the changes made to the Vagrantfile and close it.

6. **Start the virtual machine with Vagrant**: Execute the following command to start the virtual machine with Vagrant, which will configure the machine according to the specifications provided in the Vagrantfile:
   ```
   vagrant up
   ```

7. **Access the virtual machine via SSH with Vagrant** : Once the virtual machine is up and running, you can connect to it via SSH using the following command:
   ```
   vagrant ssh
   ```

8. **Access the application from your local PC**: Verify that you can access the application from your local PC by entering http://10.0.0.10 in a web browser.

9. **Stop the virtual machine**: When you have finished testing the application, stop the virtual machine with the following command:
   ```
   vagrant halt
   ```

10. **Delete the virtual machine and its associated resources**: Finally, delete the virtual machine and all its associated resources with the following command:
    ```
    vagrant destroy
    ```

