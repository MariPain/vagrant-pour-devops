### Summary (English)

#### Project Context
As a DevOps System Administrator, you must be able to quickly deploy infrastructures and test environments.

#### Pedagogical Modalities
1. **Discovering Vagrant:**

   - Install Vagrant on your Windows system.
   - Create a lab directory for testing and navigate to it.
   - Execute the following commands in order and understand the purpose of each command.

     - Vagrant init
     - Vagrant validate
     - Vagrant up
     - Vagrant status
     - Vagrant global-status
     - Vagrant ssh
     - Vagrant halt
     - Vagrant destroy
     - Vagrant box add ubuntu/trusty64
     - Vagrant init ubuntu/trusty64 -h

2. You will have 9 labs to complete (2 possible bonus Lab-10 & 11).

   **Note:** Vagrant requires a hypervisor to function.

#### Evaluation Modalities
Written report (document in your Vagrant folder on GitLab) plus all labs in GitLab.

#### Deliverables
Completed GitLab repository with all labs.

#### Performance Criteria
All labs functional.

### Résumé (Français)

#### Contexte du Projet
En tant qu'administrateur système DevOps, vous devez être capable de déployer rapidement des infrastructures et des environnements de test.

#### Modalités Pédagogiques
1. **Découverte Vagrant :**

   - Installez Vagrant sur votre système Windows.
   - Créez un répertoire de laboratoire pour les tests et déplacez-vous dedans.
   - Exécutez les commandes suivantes dans l'ordre et comprenez le but de chaque commande.

     - Vagrant init
     - Vagrant validate
     - Vagrant up
     - Vagrant status
     - Vagrant global-status
     - Vagrant ssh
     - Vagrant halt
     - Vagrant destroy
     - Vagrant box add ubuntu/trusty64
     - Vagrant init ubuntu/trusty64 -h

2. Vous aurez 9 laboratoires à réaliser (2 bonus possibles Lab-10 & 11).

   **Note :** Vagrant nécessite un hyperviseur pour fonctionner.

#### Modalités d'Évaluation
Rapport écrit (document dans votre dossier Vagrant sur GitLab) plus tous les laboratoires sur GitLab.

#### Livrables
Répertoire GitLab complété avec tous les laboratoires.

#### Critères de Performance
Tous les laboratoires fonctionnels.
