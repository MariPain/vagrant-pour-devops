### Version française :

1. **Création du dossier et navigation** :
   ```bash
   mkdir lab-2
   cd lab-2
   ```

2. **Initialisation de Vagrant** :
   ```bash
   vagrant box add ubuntu/trusty64 --box-version 20190425.0.0

   vagrant init ubuntu/trusty64 --box-version 20190425.0.0
   ```

3. **Démarrage de la machine virtuelle** :
   ```bash
   vagrant up
   ```

4. **Connexion SSH à la machine virtuelle** :
   ```bash
   vagrant ssh
   ```

5. **Installation de Nginx sur la machine virtuelle** :
   ```bash
   sudo apt-get update
   sudo apt-get install nginx
   ```

6. **Démarrage du service Nginx et activation pour un démarrage automatique** :
   ```bash
   sudo service nginx start

   ```

7. **Copie du nom de la machine virtuelle depuis VirtualBox** :
   - Ouvrez VirtualBox.
   - Trouvez le nom de la machine virtuelle créée.

8. **Emballage de la machine virtuelle** :
   ```bash
   vagrant package --base virtual-machine --output nginx.box
   ```

   Remplacez `virtual-machine` par le nom de votre machine virtuelle dans VirtualBox et `nginx` par le nom souhaité pour votre fichier d'emballage.

9. **Création d'un compte et publication sur Vagrant Cloud** :
   - Créez un compte sur Vagrant Cloud si vous n'en avez pas.
   - Téléversez le paquet sur Vagrant Cloud via l'interface web de Vagrant Cloud, en suivant les instructions fournies sur leur site.

10. **Vérification de la publication sur Vagrant Cloud** :
    Une fois que vous avez téléversé le paquet sur Vagrant Cloud, vérifiez qu'il a été publié avec succès en utilisant la commande suivante :
    ```bash
    vagrant cloud box show your-username/your-box-name
    ```

    Remplacez `your-username` par votre nom d'utilisateur sur Vagrant Cloud et `your-box-name` par le nom de votre boîte sur Vagrant Cloud.

Ces étapes vous aideront à configurer votre environnement Vagrant et à télécharger le paquet sur Vagrant Cloud correctement. Assurez-vous de remplacer les noms et les versions selon votre configuration spécifique.

### English Version:

1. **Creating the folder and navigating**:
   ```bash
   mkdir lab-2
   cd lab-2
   ```

2. **Initializing Vagrant**:
   ```bash
   vagrant init ubuntu/trusty64 --box-version 20190425.0.0
   ```

3. **Starting the virtual machine**:
   ```bash
   vagrant up
   ```

4. **SSH connection to the virtual machine**:
   ```bash
   vagrant ssh
   ```

5. **Installing Nginx on the virtual machine**:
   ```bash
   sudo apt-get update
   sudo apt-get install nginx
   ```

6. **Starting the Nginx service and enabling it to start automatically**:
   ```bash
   sudo service nginx start
   ```

7. **Copying the name of the virtual machine from VirtualBox**:
   - Open VirtualBox.
   - Find the name of the created virtual machine.

8. **Packaging the virtual machine**:
   ```bash
   vagrant package --base virtual-machine --output nginx.box
   ```

   Replace `virtual-machine` with the name of your virtual machine in VirtualBox and `nginx` with the desired name for your package file.

9. **Creating an account and publishing on Vagrant Cloud**:
   - Create an account on Vagrant Cloud if you don't have one.
   - Upload the package to Vagrant Cloud through the Vagrant Cloud web interface, following the instructions provided on their site.

10. **Verifying the publication on Vagrant Cloud**:
    Once you have uploaded the package to Vagrant Cloud, verify that it has been successfully published using the following command:
    ```bash
    vagrant cloud box show your-username/your-box-name
    ```

    Replace `your-username` with your username on Vagrant Cloud and `your-box-name` with the name of your box on Vagrant Cloud.

