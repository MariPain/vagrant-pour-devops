# Lab-9 : Installation de nginx à l’aide d’ansible

Créez Lab-9 avec une machine virtuel en utilisant Vagrant et configurez-les selon vos spécifications :

1. **Créez un dossier lab-8** :
    ```
    mkdir lab-8
    cd lab-8
    ```
2. **Ajoutez les boîtes Vagrant nécessaires** : Utilisez la commande `vagrant box add` pour ajouter les boîtes Vagrant à votre environnement local. Assurez-vous d'ajouter la boîte "ubuntu/xenial64" ou toute autre boîte que vous avez choisie.

   ```
   vagrant box add ubuntu/xenial64
   ```